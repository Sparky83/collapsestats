let globalData = [];

function getQuery(after){
    let subreddit = "collapse";
    let is_self = "false";
    // let is_video = "false";
    let size = "500";
    let fields = "domain,score,id";
    let domain = "!bit.ly,!np.reddit.com,!i.redd.it,!reddit.com,!v.redd.it,!iy.reddit.com,!i.reddituploads.com,!youtube.com,!m.youtube.com,!imgur.com,!youtu.be,!i.imgur.com,!google.com,!translate.google.it,!gfycat.com";
    let is_reddit_media_domain = "false";
    let minScore = getMinScore(after);
    let query = [];
    let before = new Date();
    before.setTime(after);
    before.setMonth(before.getMonth() + 1);
    query.push("https://api.pushshift.io/reddit/submission/search?subreddit=" + subreddit);
    query.push("is_self=" + is_self);
    query.push("is_reddit_media_domain=" + is_reddit_media_domain);
    query.push("sort_type=score");
    query.push("sort=desc");
    query.push("size=" + size);
    query.push("fields=" + fields);
    query.push("domain=" + domain);
    query.push("score=>" + minScore);
    query.push("after=" + Math.floor(after/1000));
    query.push("before=" + Math.floor(before.getTime()/1000));
    query = query.join("&");
    return query;
}

function getMinScore(after){
    if(after.getFullYear() < 2010){
        return 0;
    } else if(after.getFullYear() < 2014){
        return 10;
    } else {
        return 20;
    }
    return 0;
}

function getCORS(url, success) {
    var xhr = new XMLHttpRequest();
    if (!('withCredentials' in xhr)) xhr = new XDomainRequest(); // fix IE8/9
    xhr.open('GET', url);
    xhr.onload = success;
    xhr.send();
    return xhr;
}

function fetch(){
    let start = new Date();
    start.setTime(1209736159000);
    
    let query = getQuery(start);
    getData(query, start);
}

function getData(query,start){
    getCORS(query,(request)=>{
        let time = Math.floor(start.getTime()/1000);
        if(time < Math.floor((new Date()).getTime()/1000)){
            let div = document.querySelector(".json");
            div.innerHTML = div.innerHTML + start.toUTCString() + "<br>";
            let result = JSON.parse(request.currentTarget.response);
            let tempData = {"time": time, "data": result.data};
            globalData.push(tempData);
            start.setMonth(start.getMonth()+1);
            let query = getQuery(start);
            getData(query,start);
        }
    });
}

let debugStart = new Date();
// debugStart.setTime(1209736159000); // 02. Juli(?) 2008
debugStart.setTime(1529625049000);

function debug(){
    let data = getQuery(debugStart);
    getCORS(data,(request)=>{
        document.querySelector(".json").innerHTML = "<br><a href='"+data+"' target='_blank'>" +data+ "</a><br>" + debugStart.toUTCString() + "<br><br>" + request.currentTarget.response;
    });
    debugStart.setMonth(debugStart.getMonth()+1);
}