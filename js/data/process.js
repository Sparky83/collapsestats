
function count(data, resultVar, time){
    let result = {};
    if(resultVar !== null && typeof resultVar !== "undefined"){
        result = resultVar;
    }
    for(entry of data){
        let domain = entry.domain;
        domain = normalizeDomain(domain);
        domain = eliminateSubdomain(domain);
        if(domain !== "removed"){
            let domainCount = result[domain];
            if(typeof domainCount === "undefined"){
                result[domain] = {"count": 0, "score": 0};
            }
            result[domain].count++;
            result[domain].score += entry.score;
        }
    }
    // filter out domains that do not fit criteria
    let removables = [];
    for(entry in result){
        if(time >= 1538488150 || time <= 1549118969){
            break;
        }
        if((result[entry].count <= 1 && result[entry].score < 100) || (result[entry].score/result[entry].count < 5)){
            removables.push(entry);
        }
    }
    console.log("size: " + removables.length);
    // finally remove the domains that are to removed
    for(remove of removables){
        console.log("trara: " + remove);
        delete result[remove];
    }
    return result;
}

function normalizeDomain(domain){
    if(domain.startsWith("m.")){
        domain = domain.substr(2);
    } else if(domain.startsWith("mobile.")){
        domain = domain.substr(7);
    } else if(domain.includes("ampproject.org")){
        domain = domain.substring(0, domain.indexOf("."));
        domain = domain.replace(/-/g, '.');
        if(domain.startsWith("amp.") || domain.startsWith("amp-")){
            domain = domain.substr(4);
        }
    } else if(domain.includes(".bp.blogspot")){
        domain = "blogspot.com";
    } else if(domain.includes("rackcdn")){
        domain = "removed";
    }else if(domain.includes("archive.org")){
        domain = "removed";
    }else if(domain.includes(".tumblr.")){
        domain = "removed";
    }else if(domain.includes("giphy.com")){
        domain = "removed";
    } else if(domain.includes("fastly.net")){
        domain = "removed";
    } else if(domain.includes("209.217.209.33")){
        domain = "removed";
    }else if(domain.startsWith("self.")){
        domain = "removed";
    } else if(domain == "cleanenergyaction.files.wordpress.com"){
        domain = "cleanenergyaction.wordpress.com";
    } else if(domain == "las493energy.files.wordpress.com"){
        domain = "las493energy.wordpress.com";
    } else if(domain == "twistedsifter.files.wordpress.com"){
        domain = "twistedsifter.com";
    } else if(domain == "pricetags.files.wordpress.com"){
        domain = "pricetags.ca";
    } else if(domain == "grist.files.wordpress.com"){
        domain = "grist.org";
    } else if(domain == "collapseofindustrialcivilization.files.wordpress.com"){
        domain = "collapseofindustrialcivilization.com";
    } else if(domain == ""){
        domain = "";
    } else if(domain == ""){
        domain = "";
    } else if(domain == ""){
        domain = "";
    } else if(domain == ""){
        domain = "";
    } else if(domain == ""){
        domain = "";
    }
    return domain;
}

function eliminateSubdomain(domain){
    if(domain === "removed"){
        return domain;
    }
    if(domain.split(".") == 2){
        return domain;
    }
    let parts = domain.split(".");
    if(domain.endsWith("com.au") || 
            domain.endsWith("co.uk") || 
            domain.endsWith("net.au")){

        domain = parts[parts.length - 3] + "." + parts[parts.length - 2] + "." + parts[parts.length - 1];
    } else {
        domain = parts[parts.length - 2] + "." + parts[parts.length - 1];
    }
    return domain;
}

function countMonths(){
    let result = [];
    for(month of allData){
        result.push({
            time: month.time,
            data: transformObjectToArray(count(month.data, null, month.time)),
        });
    }
    printToJson(result);
    return result;
}

function countAll(){
    let result = {};
    for(month of allData){
        result = count(month.data, result);
    }
    return result;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function compareDomainsByCount( a, b ) {
    if ( a.count > b.count ){
      return -1;
    }
    if ( a.count < b.count ){
      return 1;
    }
    return 0;
  }

function transformObjectToArray(obj){
    var sortable = [];
    for (entry in obj) {
        sortable.push({
            "domain": entry, "count": obj[entry].count, "score": obj[entry].score
        });
    }
    return sortable;
}

function debug(){
    let data = countAll();
    let jsonElem = document.querySelector(".json");
    let text = [];
    let dataArray = transformObjectToArray(data);
    dataArray.sort(compareDomainsByCount);

    text.push(dataArray.length + "<br>");
    let displayCount = 0;
    for(let i = 0; i < dataArray.length; i++){
        let entry = dataArray[i];
        if(entry.domain.split(".").length >= 1 && entry.count > 1 && entry.score > 10){
            text.push("<b>" + entry.domain + "</b><br>count: " + entry.count + "; score: " + entry.score + "<br>");
            displayCount++;
        }
    }
    text = text.join("<br>");
    text += "<br>displayCount: " + displayCount + "<br>";

    jsonElem.innerHTML = text;
}

function printToJson(object){
    let jsonElem = document.querySelector(".json");
    let json = JSON.stringify(object);
    jsonElem.innerHTML = json;
}