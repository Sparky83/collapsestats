let diagram = {
    lastTime: 0,
    speed: 1200,
    index: 0, // current position in dataset
    maxIndex: 0, // final position of dataset
    data: [], // e.g.: {label: nytimes.com, value: 37} - label is treated as ID, so it has to be unique
    numDisplayed: 10,
    maximum: 0,
    colorIndex: 1,
    colorIndexMax: 20,
    source: [],
    averageLookout: 2,
    averageBuffer: [],
    stopped: false,
    processID: 0,
    date: 0,
    idPrefix: "ID_",
}

function init(src){
    diagram.source = src;
    diagram.maxIndex = src.length - 1;
    diagram.index = diagram.averageLookout;
    prefillDataBuffer();
}

function update(){
    let time = new Date();
    time = time.getTime();
    let delta = time - diagram.lastTime;
    if(delta > diagram.speed && diagram.index <= diagram.maxIndex){
        lastTime = time;
        // update value buffer (remove oldest, add next)
        updateBuffer();
        // read out current displayed values (read and sort values, then copy)
        diagram.data = getAveragedBufferData();
        // get topvalue
        let maxElem = diagram.data[0];
        if(typeof maxElem !== "undefined"){
            diagram.maximum = maxElem.value;
        } else {
            diagram.maximum = 1;
        }
        // increment dataset index (use next point in time)
        diagram.index++; 
    }
}

function render(){
    // remove dropout-html
    removeElements();
    setBarsUntouched();
    showDate();
    let delay = 5;

    for(let i = 0; i < diagram.data.length && i < 20; i++){
        let newElem = false;
        let entry = diagram.data[i];
        let elem = getBarById(entry.label);
        
        // create html for new entries
        if(elem === null){ 
            elem = createElement(entry.label);
            newElem = true;
        }

        // update data-attribute values on html representations of entries
        // for correct display positions
        if(newElem){
            setTimeout(move,delay,entry.label, i+1);
            setTimeout(setValue,delay,entry.label, entry.value);
        } else {
            move(entry.label, i+1);
            setValue(entry.label, entry.value);
        }
    }
    // The bars that are still untouched have to be removed
    setTimeout(removeUntouched,delay);
}

function loop(procID){
    // The processID prevents the loop from being called multiple
    // times when repeatedly pausing and restarting. Previous 
    // calls to setTimeout are invalidated. (Not working in IE9)
    if(procID == diagram.processID){
        update();
        render();
        setTimeout(loop, 2000, diagram.processID);
    }
}

function pause(){
    if(!diagram.stopped){
        diagram.stopped = true;
        diagram.processID++;
    }
    else {
        diagram.stopped = false;
        loop(diagram.processID);
    }
}

function showDate(){
    let date = diagram.date;
    let time = new Date();
    time.setTime(date * 1000);
    document.querySelector("#datetext").textContent = time.toDateString();
}

function move(id, pos){
    let elem = getBarById(id);
    elem.setAttribute("data-touched","true");
    if(pos <= diagram.numDisplayed){
        elem.setAttribute("data-pos", pos);
    } else {
        elem.setAttribute("data-pos", "out");
    }
}

function setBarsUntouched(){
    let elems = document.querySelectorAll(".bar");
    elems.forEach(function(elem){
        elem.setAttribute("data-touched","false");
    });
}

function removeUntouched(){
    let elems = document.querySelectorAll("[data-touched='false']");
    elems.forEach(function(elem){
        elem.setAttribute("data-pos", "out");
    });
}

function setValue(id, value){
    let elem = getBarById(id);
    elem.setAttribute("data-value", value);
    let relValue = Math.ceil(value/diagram.maximum * 100);
    elem.setAttribute("style", "width: " + relValue + "%");
}

function createElement(value){
    let div = document.createElement("div");
    div.setAttribute("class","bar bar" + diagram.colorIndex);
    div.setAttribute("id", diagram.idPrefix + value);
    div.setAttribute("style", "width: 50px");
    div.setAttribute("data-pos", "out");
    div.setAttribute("data-label", value);
    diagram.colorIndex++;
    if(diagram.colorIndex > diagram.colorIndexMax){
        diagram.colorIndex = 1;
    }
    document.querySelector(".diagram").appendChild(div);
    return div;
}

function removeElements(){
    let outElems = document.querySelectorAll("[data-pos='out']");
    outElems.forEach(function(elem){
        elem.parentNode.removeChild(elem);
    });
}

function getValues(index){
    if(typeof index === "undefined"){
        index = diagram.index + diagram.averageLookout;
    }
    let result = [];
    let newData = diagram.source[index];
    let time = newData.time;
    diagram.date = time;
    let domains =  newData.data;
    // ############################################################################################# DER FILTER
    if(domains.length > 100){
        domains = domains.filter(function(elem){
            return elem.count > 1;
        });
    }

    domains.sort(compareDomainsByCount);
    for(let i = 0; i < domains.length; i++){
        result.push({"label": domains[i].domain, "value": domains[i].count});
    }
    return result;
    // [{time, data: {domain: {count, score}}}]
}

function prefillDataBuffer(){
    // for the average lookout, take all data except the outmost current
    let lookout = diagram.averageLookout;
    diagram.averageBuffer[0] = null;
    let bufferIndex = 1;
    for(let i = diagram.index - lookout; i < diagram.index + lookout; i++){
        diagram.averageBuffer[bufferIndex++] = getValues(i);
    }
}

function updateBuffer(){
    diagram.averageBuffer = diagram.averageBuffer.splice(1);
    let newData = getValues(diagram.index + diagram.averageLookout);
    diagram.averageBuffer.push(newData);
}

function getAveragedBufferData(){
    let result = {};
    for(bufferEntry of diagram.averageBuffer){
        for(dataEntry of bufferEntry){
            let label = dataEntry.label;
            let value = result[label];
            if(typeof value === "undefined"){
                result[label] = {"value": 0};
            }
            result[label].value += dataEntry.value;
        }
    }
    let resultArr = transformObjectToArray(result);
    // calculate average for each item
    for(entry of resultArr){
        entry.value = entry.value / diagram.averageBuffer.length;
    }
    return resultArr.sort(compareByValue);
}

function transformObjectToArray(obj){
    var sortable = [];
    for (entry in obj) {
        sortable.push({
            "label": entry, "value": obj[entry].value
        });
    }
    return sortable;
}

function compareDomainsByCount( a, b ) {
    if ( a.count > b.count ){
      return -1;
    }
    if ( a.count < b.count ){
      return 1;
    }
    return 0;
}

function compareByValue( a, b ) {
    if ( a.value > b.value ){
      return -1;
    }
    if ( a.value < b.value ){
      return 1;
    }
    return 0;
}

function getBarById(id){
    id = diagram.idPrefix + id;
    let escapedId = id.replace(/\./g,"\\.");
    return document.querySelector("#" + escapedId);
}